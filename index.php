<?php if (!defined('ABSPATH')) die(); ?>
<?php get_header(); ?>
    <main id="site-content" class="site-content" role="main">
        <?php if(have_posts()): while (have_posts()): the_post(); ?>
            <article <?php post_class('entry-content'); ?>>
                <?php the_content(); ?>
            </article>
        <?php endwhile; endif; ?>
    </main>
<?php get_footer(); ?>