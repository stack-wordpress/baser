<?php

use Baser\Vs\Theme;

if (!defined('ABSPATH')) die();

require_once get_template_directory() . '/vendor/autoload.php';
if(file_exists(get_template_directory() . '/acf.php')){
    require_once get_template_directory() . '/acf.php';
}

$themeName = 'baser';

/**
 * Set default assets versions to random, and if build assets versions exists
 * take the version number from there
 */
$assetVersion = rand();
$adminAssetVersion = rand();

if(file_exists(get_template_directory() . '/build/index.asset.php')){
    $assetVersion = include get_template_directory() . '/build/index.asset.php';
    $assetVersion = $assetVersion['version'];
}
if(file_exists(get_template_directory() . '/build/index-admin.asset.php')){
    $adminAssetVersion = include get_template_directory() . '/build/index-admin.asset.php';
    $adminAssetVersion = $adminAssetVersion['version'];
}

new Theme($themeName, $assetVersion, $adminAssetVersion);


