<?php if (!defined('ABSPATH')) die(); ?>
<?php get_header(); ?>
<main id="site-content" class="site-content" role="main">
    <section class="archive">
        <?php if(have_posts()): while (have_posts()): the_post(); ?>
            <article <?php post_class(); ?>>
                <header>
                    <h2><?php the_title(); ?></h2>
                    <?php if(has_post_thumbnail()): ?>
                        <figure>
                            <?php the_post_thumbnail(); ?>
                        </figure>
                    <?php endif; ?>
                </header>
                <?php the_excerpt(); ?>
            </article>
        <?php endwhile; endif; ?>
    </section>
</main>
<?php get_footer(); ?>
