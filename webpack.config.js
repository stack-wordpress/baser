const fs = require('fs');
const path = require('path');
const themeDir = fs.realpathSync(process.cwd());
const defaultConfig = require("./node_modules/@wordpress/scripts/config/webpack.config");
const browserSyncPlugin = require('browser-sync-webpack-plugin');
const resolvePath = relativePath => path.resolve(themeDir, relativePath);

module.exports = {
    ...defaultConfig,
    plugins: [...defaultConfig.plugins,
        new browserSyncPlugin({
            host: 'localhost',
            port: 8083,
            proxy: 'http://localhost:8181',
            files: [
                resolvePath('./build/*.css'),
                resolvePath('./build/*.js'),
                resolvePath('./**/*.php'),
            ],
        })
    ]
};



   