![Logo](images/vs-logo.png)

# WordPress starter theme
This is a class based theme using autoloader with composer autoload and psr-4 standards. This is supporting building
themes using the WordPress editor (also called Gutenberg). So by default there is no support for Elementor or other 
page builders.

## What is included
* Bootstrap 5.1 support
* Slick slider support
* Prepared for building custom WordPress editor blocks
* Prepared for Acf
* WooCommerce Support

## Requirements
* composer
* npm 8.5.0 or higher
* node 16.14.2 or higher
* PHP 8 or higher
* WordPress 6 or higher

## Installation
1. run `composer install`
2. run `npm install`
3. run `npm run build`

## Using this theme
### Enable bootstrap
In `function.php` where you initiate the theme class, you add a boolean true as the fourth parameter,
so it will look like this:

`new Theme($themeName, $assetVersion, $adminAssetVersion, true);`

Bootstrap is currently loading thru cdn, and it will be loaded in before your own styling and javascript.
This way it is easy to extend or override javascript and css
### Enable Slick
In `function.php` where you initiate the theme class, you add a boolean true as the fifth parameter,
so it will look like this:

`new Theme($themeName, $assetVersion, $adminAssetVersion, false, true);`

If you have bootstrap enable it will look like this:

`new Theme($themeName, $assetVersion, $adminAssetVersion, true, true);`

Slick is currently loading thru cdn, and it will be loaded in before your own styling and javascript. This way
it is easy to extend or override javascript and css

# Structure
### Images 
Here you will add all the images that you want in the theme, a good example of this is a search icon.
### src
Here will all your php classes go within directories.
### src/assets
Here is where the all your javascript and scss/css files will go
### src/index.js
Here you will include if you add more than one Javascript file in the src/assets/js folder. If they are not included
here they will not be compiled into the build folder.