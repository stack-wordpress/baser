<?php

namespace Baser\Vs;

class Theme
{

    /** @var string $themeName */
    private string $themeName;

    /** @var string $assetVersion */
    private string $assetVersion;

    /** @var string $adminAssetVersion */
    private string $adminAssetVersion;

    /** @var bool $enableWooCommerce */
    private bool $enableWooCommerce;

    /** @var bool $enableBootstrap */
    private bool $enableBootstrap;

    /** @var bool $enableSlick */
    private bool $enableSlick;

    /**
     * Theme constructor.
     * Asset version is needed to get a clean version for every build
     *
     * @param string $themeName
     * @param string $assetVersion
     * @param string $adminAssetVersion
     * @param bool $enableWooCommerce
     * @param bool $enableBootstrap;
     * @param bool $enableSlick
     */
    public function __construct(string $themeName, string $assetVersion, string $adminAssetVersion, bool $enableWooCommerce = false, bool $enableBootstrap = false, bool $enableSlick = false)
    {
        $this->themeName         = $themeName;
        $this->assetVersion      = $assetVersion;
        $this->adminAssetVersion = $adminAssetVersion;
        $this->enableWooCommerce = $enableWooCommerce;
        $this->enableBootstrap   = $enableBootstrap;
        $this->enableSlick       = $enableSlick;

        add_action('init', [$this, 'initTheme']);
        add_action('admin_enqueue_scripts', [$this, 'registerAdminScripts']);
        add_action('after_setup_theme', [$this, 'afterThemeInit']);
        add_action( 'wp_enqueue_scripts', [$this, 'my_theme_enqueue_block_styles'] );
        add_action( 'admin_init', [$this, 'my_theme_enqueue_block_styles'] );
        add_filter( 'should_load_separate_core_block_assets', '__return_true' );
    }

    public function initTheme():void
    {
        $this->registerNavigation();
        $this->addThemeSupport();
        $this->registerStyles();
        $this->registerScripts();
    }

    public function my_theme_enqueue_block_styles():void
    {
        // An array of blocks.
        $styled_blocks = [ 'paragraph' ];


        foreach ( $styled_blocks as $block_name ) {
            // Get the stylesheet handle. This is backwards-compatible and checks the
            // availability of the `wp_should_load_separate_core_block_assets` function,
            // and whether we want to load separate styles per-block or not.
            $handle = (
                function_exists( 'wp_should_load_separate_core_block_assets' ) &&
                wp_should_load_separate_core_block_assets()
            ) ? "wp-block-$block_name" : 'wp-block-library';
            if(file_exists(get_theme_file_path( "build/$block_name.css" ))){
                if(file_exists(get_template_directory() . "/build/$block_name.asset.php")){
                    $assetVersion = include get_template_directory() . "/build/$block_name.asset.php";
                    $assetVersion = $assetVersion['version'];
                }else{
                    $assetVersion = rand();
                }
                wp_enqueue_style($handle, get_theme_file_path( "build/$block_name.css" ), '', $assetVersion);
                // Get the styles.
                $styles = file_get_contents( get_theme_file_path( "build/$block_name.css" ) );
                // Add frontend styles.
                wp_add_inline_style( $handle, $styles );
            }
            if ( file_exists( get_theme_file_path( "styles/blocks/$block_name-editor.min.css" ) ) ) {
                add_editor_style( "styles/blocks/$block_name-editor.min.css" );
            }
        }
    }

    public function registerAdminScripts():void
    {
        if (is_admin()) {
            wp_enqueue_script(
                $this->themeName . '-admin-script',
                get_bloginfo('template_directory') . '/build/index-admin.js',
                ['wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore'],
                $this->adminAssetVersion
            );
            wp_enqueue_style(
                $this->themeName . '-admin-style',
                get_bloginfo('template_directory') . '/build/index-admin.css',
                [],
                $this->adminAssetVersion
            );

        }
    }

    public function afterThemeInit():void
    {
        /**
         * Add support for theme translations
         *
         *
         * @link https://developer.wordpress.org/reference/functions/load_theme_textdomain/
         */
        load_theme_textdomain('sb', get_template_directory() . '/languages');

        /** Enable support for Thumbnails on posts and pages */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1568, 9999);

        /** Enable rss feed for default posts and comments */
        add_theme_support('automatic-feed-links');

        /**
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'script',
            'style',
        ]);

        /** Editor color palette */
        add_theme_support('editor-color-palette');

        /** Adding Woocommerce Theme support */
        if($this->enableWooCommerce){
            add_theme_support( 'woocommerce' );
            add_theme_support( 'wc-product-gallery-zoom' );
            add_theme_support( 'wc-product-gallery-lightbox' );
            add_theme_support( 'wc-product-gallery-slider' );
        }
    }

    protected function registerNavigation():void
    {
        /**
         * Register Navigation locations, this theme has two locations as default,
         * to add more locations, add a new line with the function "register_nav_manu()"
         *
         * @link https://codex.wordpress.org/Function_Reference/register_nav_menu
         */
        register_nav_menu('primary-menu', __('The primary navigation', 'vs'));
        register_nav_menu('footer-menu', __('Footer navigation', 'vs'));

    }

    protected function addThemeSupport():void
    {
        /**
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */

        $defaults = [
            'height'      => 100,
            'width'       => 200,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => ['site-title', 'site-description'],
        ];
        add_theme_support('custom-logo', $defaults);

        /**
         * Core blocks include default styles. The styles are enqueued for editing but are not enqueued for
         * viewing unless the theme opts-in to the core styles. If you’d like to use default styles in your theme,
         * add theme support for wp-block-styles
         *
         * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#default-block-styles
         */
        add_theme_support('wp-block-styles');

        /**
         * Some blocks such as the image block have the possibility to define a “wide” or “full”
         * alignment by adding the corresponding classname to the block’s wrapper
         * ( alignwide or alignfull ). Add support for it
         *
         * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#wide-alignment
         */
        add_theme_support('align-wide');

        /**
         * Add support for editor styles
         *
         * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#editor-styles
         */
        add_theme_support('editor-styles');
        add_editor_style('style-editor.css');

        /**
         * To make the content resize and keep its aspect ratio, the <body> element needs
         * the wp-embed-responsive class. This is not set by default,
         * and requires the theme to opt in to the responsive-embeds feature
         *
         * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#responsive-embedded-content
         */
        add_theme_support('responsive-embeds');

    }


    protected function registerStyles():void
    {
        if(!is_admin()){
            $requires = [];
            if($this->enableBootstrap){
                $requires[] = 'bootstrap';
                wp_enqueue_style(
                    'bootstrap',
                    '//cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css',
                    [],
                    '5.1.0'
                );
            }
            if($this->enableSlick){
                $requires[] = 'slick';
                wp_enqueue_style(
                    'slick',
                    '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css',
                    [],
                    '1.8.1'
                );
            }
            wp_enqueue_style(
                $this->themeName . '-style',
                get_bloginfo('template_directory') . '/build/style-index.css',
                $requires,
                $this->assetVersion
            );
        }
    }


    protected function registerScripts():void
    {
        /** Only load if not in admin */
        if (!is_admin()) {
            $requires = ['jquery'];
            if($this->enableBootstrap){
                wp_enqueue_script(
                    'bootstrap',
                    '//cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js',
                    $requires,
                    '5.1.0',
                    true
                );
                $requires[] = 'bootstrap';
            }
            if($this->enableSlick){
                $requires[] = 'slick';
                wp_enqueue_script(
                    'slick',
                    '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js',
                    [],
                    '1.8.1',
                    true
                );
            }
            wp_enqueue_script(
                $this->themeName . '-script',
                get_bloginfo('template_directory') . '/build/index.js',
                $requires,
                $this->assetVersion,
                true
            );

        }
    }
}